<?php
/**
* [ 'name' => 'Element Name',
*   'url' => 'Element Link',
*   'icon' => 'Element Icon', // From font awesome library. Ex: eye, heart, inbox, etc.
*   'children' => [ array of elements ],
* ]
*/
return [
    [
        'name' => 'Members',
        'url' => 'dashboard.members.index',
        'icon' => 'user',
    ],
    [
      'name' => 'Configs',
      'url'  => 'dashboard.configs.edit',
      'icon' => 'cogs',
    ],
    [
        'name' => 'Images',
        'url' => 'dashboard.images.index',
        'icon' => 'images', 
    ],
    [
        'name' => 'Events',
        'url' => 'dashboard.events.index',
        'icon' => 'users',
    ],
    [
        'name' => 'Speakers',
        'url' => 'dashboard.speakers.index',
        'icon' => 'user-ninja',
    ],
];