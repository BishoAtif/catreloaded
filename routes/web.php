<?php


Route::group(['namespace' => 'Web', 'as' => 'web.'], function () {
    Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home']);

    Route::get('about-us', ['uses' => 'AboutUsController@index', 'as' => 'about-us']);
    Route::get('circles', ['uses' => 'CircleController@index', 'as' => 'circles']);

    Route::get('scopes', ['uses' => 'ScopeController@index', 'as' => 'scopes.index']);
    Route::get('scopes/{slug}', ['uses' => 'ScopeController@show', 'as' => 'scopes.show']);

    Route::get('events', ['uses' => 'EventController@index', 'as' => 'events.index']);
    Route::get('events/{slug}', ['uses' => 'EventController@show', 'as' => 'events.show']);

    Route::get('gold-members', ['uses' => 'MemberController@gold', 'as' => 'gold-members']);
    Route::get('members/{slug}', ['uses' => 'MemberController@show', 'as' => 'members.show']);

    Route::get('calender', ['uses' => 'Test@show', 'as' => 'calender']);
    Route::get('contact-us', ['uses' => 'Test@show', 'as' => 'contact-us']);
});


Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.', 'namespace' => 'Dashboard'], function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');

    Route::group(['middleware' => 'auth'], function () {
        Route::get('home', ['uses' => 'HomeController@index', 'as' => 'home']);

        Route::post('logout', 'Auth\LoginController@logout')->name('logout');

        Route::get('configs', ['uses' => 'ConfigController@edit', 'as' => 'configs.edit']);
        Route::post('configs', ['uses' => 'ConfigController@store', 'as' => 'configs.store']);
        Route::resource('members', 'MemberController');
        Route::resource('events', 'EventController');
        Route::resource('images', 'ImageController');
        Route::resource('speakers', 'SpeakerController');

        Route::group(['prefix' => 'ajax', 'as' => 'ajax.', 'namespace' => 'Ajax'], function () {
            Route::resource('members', 'MemberController');
            Route::resource('events', 'EventController');
            Route::get('images', ['uses' => 'ImageController@index', 'as' => 'images.index']);
            Route::get('images/search', ['uses' => 'ImageController@search', 'as' => 'images.search']);
            Route::get('speakers', ['uses' => 'SpeakerController@index', 'as' => 'speakers.index']);
            Route::get('speakers/search', ['uses' => 'SpeakerController@search', 'as' => 'speakers.search']);
        });
    });
});
