requirejs.config({
    baseUrl: '/dashboard/js/lib',
    paths: {
        'main': '../adminlte.min',
        'jquery': '//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min',
        'jquerySlimscroll': '//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min',
        'bootstrap': '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min',
        'fastclick': '//cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min',
        'dataTables': '//cdn.datatables.net/1.10.18/js/jquery.dataTables.min',
        'datatables.net': '//cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min',
        'datetimepicker': '//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min',
        'ckeditor': '//cdn.ckeditor.com/4.10.0/standard/ckeditor',
        'moment': '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min',
        'select2': '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min'
    },
    shim: {
        'main': {
            deps: ['jquery']
        },
        'datatables.net': {
            deps: ['dataTables']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'jquerySlimscroll': {
            deps: ['jquery']
        },
        'datetimepicker': {
            deps: ['moment']
        }
    }
});

require([
    'jquery', 'jquerySlimscroll', 'bootstrap', 'fastclick', 'main'
]);
