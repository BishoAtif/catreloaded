require(['jquery', 'select2'], function ($) {

  var select2Object = {
    ajax: {
      url: url,
      dataType: 'json',
      data: function (params) {
        var query = {
          search: params.term
        }
        return query;
      },
      processResults: function (data) {
        data = $.map(data, function (value, key) {
          return { id: key, text: value };
        });
        return {
          results: data
        };
      }
    }
  };

  $('.repeated-inputs').on('click', 'button.remove-input', function (event) {
    var numOfInputs = $('div.repeated-inputs .row:has(div.form-group)').length;
    if (numOfInputs === 1) {
      alert("There is only one element, Please don't remove it :'(");
      return;
    }
    this.closest('.row').remove();
  });

  $('.add-input').click(function (url) {
    var input = $('.repeated-inputs .row:has(div.form-group)').last();
    var selectInputNameTemplate = input.find('select')[0].dataset.nameTemplate;
    input.find('select').select2('destroy');
    var clonedInput = input.clone(true, true);
    clonedInput.find('.form-control').each(function (index, el) {
      $(this).val('');
    });

    clonedInput.insertAfter(input);
    $(clonedInput).find('select').replaceWith(`<select class="form-control image" data-name-template="${selectInputNameTemplate}"><option></option></select>`);
    $('select').select2(select2Object);

    $.each($('.repeated-inputs .row'), function (rowKey, row) {
      $.each($(row).find('select, input'), function (inputKey, input) {
        input.name = selectInputNameTemplate.replace('{}', rowKey);
      });
    });
  });

  $('select').select2(select2Object);
});