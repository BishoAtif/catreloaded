<?php

use Illuminate\Database\Seeder;
use App\Entities\Config;
use App\Enumerations\ConfigKeys;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Config::firstOrCreate([
            'key' => ConfigKeys::SLIDER,
            'value' => 'https://www.blog.catreloaded.org',
            'image_id' => null
        ]);
    }
}
