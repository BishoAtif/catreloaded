<?php

use Illuminate\Database\Seeder;
use App\Entities\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'name' => 'cat',
            'email' => 'cat@cat.com',
            'password' => bcrypt(123456),
        ]);
    }
}
