<?php
namespace App\Helpers;

class MenuHelper
{
    private static function getMenuElements()
    {
        return config('menu');
    }

    public static function formatMenu()
    {
        $menu = self::getMenuElements();
        self::formatElement($menu);
    }

    public static function formatElement($menu)
    {
        foreach ($menu as $element) {
            if (!array_key_exists('children', $element)) {
                echo view('dashboard.partials._sidebar_child_item', compact('element'));
                continue;
            }
            echo view('dashboard.partials._sidebar_parent_item', compact('element'));
        }
    }
}
