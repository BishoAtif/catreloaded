<?php

namespace App\Helpers\Dashboard;

use App\Helpers\Dashboard\Menu;
use Auth;

class MenuFormatter
{
    private static $menuElements;

    private static function getMenu()
    {
        if (empty(self::$menuElements)) {
            self::$menuElements = config('menu.elements', []);
        }
    }

    public static function output()
    {
        if (empty(self::$menuElements)) {
            self::getMenu();
        }

        foreach (self::$menuElements as $routeName => $data) {
            $menuItem = self::getElement($routeName, $data);
            if($menuItem) {
                $menuItem->output();
            }
        }
    }

    private static function getElement($routeName, $data, $parent = null)
    {
        $label = $data[ 'label' ];
        $permissions = @$data[ 'permissions' ];
        $currentUser = Auth::user();
        $userCanView = true; //by default user can view route

        if($permissions) { 
            $userCanView = false; //if permissions exist remove view by default.
            foreach ($permissions as $permission) {
                if($currentUser->can($permission)) {
                    $userCanView = true;
                    break;
                }
            }
        }

        if(!$userCanView) {
            return;
        }

        $el = (new Menu($routeName, $label, $parent));

        if (!empty($data[ 'header' ])) {
            $el->setHeader();
            return $el;
        }

        $el->isActive();

        if (empty($data[ 'children' ])) {
            return $el;
        }

        foreach ($data[ 'children' ] as $routeName => $data) {
            $child = self::getElement($routeName, $data, $el);
            if($child) {
                $el->addChild($child);
            }
        }

        if(empty($el->children)) {
            return;
        }

        return $el;
    }
}
