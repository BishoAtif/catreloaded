<?php

namespace App\Helpers;

use Request;

class GeneralHelper
{
    public static function setActiveMenu($path, $activeClass = 'active')
    {
        print $path == Request::url() ? "active" : "";
    }
}
