<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Config;
use App\Enumerations\ConfigKeys;

/**
 * Class ConfigRepository
 * @package namespace App\Repositories;
 */
class ConfigRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Config::class;
    }

    public function get($key = null)
    {
        if (!$key) {
            return null;
        }

        return $this->findByField('key', $key);
    }

    public function getHomeSliderImages()
    {
        return $this->get(ConfigKeys::SLIDER);
    }

    public function storeSliders($sliders)
    {
        $oldSliders = $this->get(ConfigKeys::SLIDER);
        foreach($oldSliders as $slider) {
            $slider->delete();
        }

        foreach ($sliders as $slider) {
            $this->create([
                'key' => ConfigKeys::SLIDER,
                'value' => $slider['link'],
                'image_id' => $slider['image'],
            ]);
        }
    }
}
