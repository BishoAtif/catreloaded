<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Scope;

/**
 * Class ScopeRepository
 * @package namespace App\Repositories;
 */
class ScopeRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Scope::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Initializes a new query to be used by dataTables
     * 
     * @return object QueryBuilder instance to be used by dataTables
     */
    public function getAjaxIndex()
    {
        return $this->model->newQuery();
    }

    public function findScopeBySlug($slug = '')
    {
        return $this->findByField('slug', $slug)->first();
    }
}
