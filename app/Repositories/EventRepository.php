<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Event;
use Carbon\Carbon;

/**
 * Class EventRepository
 * @package namespace App\Repositories;
 */
class EventRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Event::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Initializes a new query to be used by dataTables
     *
     * @return object QueryBuilder instance to be used by dataTables
     */
    public function getAjaxIndex()
    {
        return $this->model->newQuery();
    }

    public function findEventBySlug($slug = '')
    {
        return $this->findByField('slug', $slug)->first();
    }

    public function getRecentEvents($numOfEvents = 5)
    {
        return $this->model->OrderBy('date', 'DESC')->limit($numOfEvents)->get();
    }

    public function getPastEvents()
    {
        return $this->model->where('date', '<', Carbon::now())->orderBy('date', 'DESC')->paginate(5);
    }

    public function getUpcomingEvents()
    {
        return $this->model->where('date', '>=', Carbon::now())->paginate(5);
    }
}
