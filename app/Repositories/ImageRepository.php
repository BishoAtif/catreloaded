<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Image;

/**
 * Class ImageRepository
 * @package namespace App\Repositories;
 */
class ImageRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Image::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Initializes a new query to be used by dataTables
     * 
     * @return object QueryBuilder instance to be used by dataTables
     */
    public function getAjaxIndex()
    {
        return $this->model->newQuery();
    }

    public function filterBy($filter = '')
    {
        return $this->model->where('description', 'LIKE', "%{$filter}%")->get();
    }

    public function canDelete($image = null)
    {
        if (is_null($image)) {
            return false;
        }

        if ($image->configs->count() != 0) {
            return false;
        }
        
        return true;
    }
}
