<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function index()
    {
        $members = \App\Entities\Member::current()->get();
        $member = $members;

        return view('web.about-us', compact('members'));
    }
}
