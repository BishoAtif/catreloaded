<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class CircleController extends Controller
{
    public function index()
    {
        return view('web.circles');
    }
}
