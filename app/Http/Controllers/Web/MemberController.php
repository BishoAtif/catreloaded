<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Member;
use App\Entities\Skill;

class MemberController extends Controller
{
    public function show($slug)
    {
        $member = Member::with('experiences', 'projects', 'skills')->where('slug', $slug)->first();
        
        if(!$member) {
            abort(404);
        }

        return view('web.members.show', compact('member'));
    }

    public function gold()
    {
        $members = Member::gold()->get();
        return view('web.members.gold-members', compact('members'));
    }

}
