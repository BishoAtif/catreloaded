<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ScopeRepository;

class ScopeController extends Controller
{
    public function __construct(ScopeRepository $scopes)
    {
        $this->scopes = $scopes;
    }

    public function index()
    {
        $scopes = $this->scopes->orderBy('date', 'DESC')->paginate(4);

        return view('web.scopes.index', compact('scopes'));
    }

    public function show($slug)
    {
        $scope = $this->scopes->findScopeBySlug($slug);

        
        if(!$scope) {
            abort(404);
        }

        return view('web.scopes.show', compact('scope'));
    }
}
