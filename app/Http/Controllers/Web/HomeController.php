<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EventRepository;
use App\Repositories\ConfigRepository;

class HomeController extends Controller
{
    public function __construct(EventRepository $events, ConfigRepository $configs)
    {
        $this->events = $events;
        $this->configs = $configs;
    }

    public function index()
    {
        $recentEvents = $this->events->getRecentEvents(4);
        $sliderImages = $this->configs->getHomeSliderImages();
        
        return view('web.home', compact('recentEvents', 'sliderImages'));
    }
}
