<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EventRepository;

class EventController extends Controller
{
    public function __construct(EventRepository $events)
    {
        $this->events = $events;
    }

    public function index()
    {
        $upcomingEvents = $this->events->getUpcomingEvents();
        $pastEvents = $this->events->getPastEvents();

        return view('web.events.index', compact('upcomingEvents', 'pastEvents'));
    }

    public function show($slug)
    {
        $event = $this->events->findEventBySlug($slug);

        if (!$event) {
            abort(404);
        }
        
        return view('web.events.show', compact('event'));
    }
}
