<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\EventRepository;
use App\Http\Requests\Dashboard\Event\CreateRequest;
use App\Http\Requests\Dashboard\Event\UpdateRequest;
use App\Entities\Event;

class EventController extends Controller
{
    public function __construct(EventRepository $events)
    {
        $this->events = $events;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.events.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $event = $this->events->create($request->all());
        $event->speakers()->sync($request->speakers);

        return redirect()->route('dashboard.events.index')->withSuccess('Event Added Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('dashboard.events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $event = $this->events->find($id);
        if (!$event) {
            abort(404);
        }
        
        $event->speakers()->sync($request->speakers);
        $this->events->update($request->all(), $event->id);

        return redirect()->route('dashboard.events.index')->withSuccess('Event Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->events->delete($id);

        return redirect()->route('dashboard.events.index')->withSuccess('Event Deleted Successfully');
    }
}
