<?php

namespace App\Http\Controllers\Dashboard\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SpeakerRepository;
use App\Entities\Speaker;
use Datatables;
use View;

class SpeakerController extends Controller
{
    public function __construct(SpeakerRepository $speakers)
    {
        $this->speakers = $speakers;
    }

    public function index(Request $request)
    {
        return Datatables::of($this->speakers->getAjaxIndex())
            ->addColumn('actions', function ($row) {
                $editUrl = route('dashboard.speakers.edit', $row->id);
                $deleteUrl = route('dashboard.speakers.destroy', $row->id);

                return View::make('dashboard.layouts._formActions', compact('editUrl', 'deleteUrl'));
            })
            ->addColumn('avatar', function($row){
                return $row->avatar->url('thumb');
            })
            ->make(true);
    }

    public function search(Request $request)
    {
        return $this->speakers->filterBy($request->search)->pluck('name', 'id')->toArray();
    }

}
