<?php

namespace App\Http\Controllers\Dashboard\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;
use App\Entities\Member;
use Datatables;
use View;

class MemberController extends Controller
{
    public function __construct(MemberRepository $members)
    {
        $this->members = $members;
    }

    public function index(Request $request)
    {
        return Datatables::of($this->members->getAjaxIndex())
            ->addColumn('actions', function ($row) {
                $editUrl = route('dashboard.members.edit', $row->id);
                $deleteUrl = route('dashboard.members.destroy', $row->id);
                
                return View::make('dashboard.layouts._formActions', compact('editUrl', 'deleteUrl'));
            })
            ->addColumn('joined_at', function($row){
                return $row->joined_at ? $row->joined_at->year : null;
            })
            ->make(true);
    }
}
