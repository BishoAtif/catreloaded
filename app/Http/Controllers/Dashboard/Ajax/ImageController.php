<?php

namespace App\Http\Controllers\Dashboard\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ImageRepository;
use Datatables;
use View;

class ImageController extends Controller
{
    public function __construct(ImageRepository $images)
    {
        $this->images = $images;
    }

    public function index(Request $request)
    {
        return Datatables::of($this->images->getAjaxIndex())
            ->addColumn('actions', function ($row) {
                $editUrl = route('dashboard.images.edit', $row->id);
                $deleteUrl = route('dashboard.images.destroy', $row->id);
                
                return View::make('dashboard.layouts._formActions', compact('editUrl', 'deleteUrl'));
            })
            ->addColumn('image', function($row){
                return $row->image->url('thumb');
            })
            ->make(true);
    }

    public function search(Request $request)
    {
        return $this->images->filterBy($request->search)->pluck('description', 'id')->toArray();
    }
}
