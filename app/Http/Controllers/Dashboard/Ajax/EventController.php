<?php

namespace App\Http\Controllers\Dashboard\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\EventRepository;
use App\Entities\Event;
use Datatables;
use View;

class EventController extends Controller
{
    public function __construct(EventRepository $events)
    {
        $this->events = $events;
    }

    public function index(Request $request)
    {
        return Datatables::of($this->events->getAjaxIndex()->orderBy('date', 'desc'))
            ->addColumn('actions', function ($row) {
                $editUrl = route('dashboard.events.edit', $row->id);
                $deleteUrl = route('dashboard.events.destroy', $row->id);

                return View::make('dashboard.layouts._formActions', compact('editUrl', 'deleteUrl'));
            })
            ->make(true);
    }
}
