<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\ConfigRepository;
use App\Http\Requests\Dashboard\Config\UpdateRequest;

class ConfigController extends Controller
{
    public function __construct(ConfigRepository $configs)
    {
        $this->configs = $configs;
    }

    /**
     * Show the dashboard home
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $sliders = $this->configs->getHomeSliderImages();
        return view('dashboard.configs.edit', compact('sliders'));
    }

    public function store(UpdateRequest $request)
    {
        $this->configs->storeSliders($request->sliders);
        return redirect()->back();
    }
}
