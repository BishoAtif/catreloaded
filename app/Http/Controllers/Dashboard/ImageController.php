<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\ImageRepository;
use App\Http\Requests\Dashboard\Image\CreateRequest;
use App\Http\Requests\Dashboard\Image\UpdateRequest;
use App\Entities\Image;

class ImageController extends Controller
{
    public function __construct(ImageRepository $images)
    {
        $this->images = $images;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.images.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.images.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $this->images->create($request->all());

        return redirect()->route('dashboard.images.index')->withSuccess('Image Added Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        return view('dashboard.images.edit', compact('image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $this->images->update($request->all(), $id);

        return redirect()->route('dashboard.images.index')->withSuccess('Image Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        if (!$this->images->canDelete($image)) {
            return redirect()->back()->withError('Cannot remove the image. Please check the configs attached with it and try again!');
        }

        $image->delete();

        return redirect()->route('dashboard.images.index')->withSuccess(' Image Deleted Successfully');
    }
}
