<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\SpeakerRepository;
use App\Http\Requests\Dashboard\Speaker\CreateRequest;
use App\Http\Requests\Dashboard\Speaker\UpdateRequest;
use App\Entities\Speaker;

class SpeakerController extends Controller
{
    public function __construct(SpeakerRepository $speakers)
    {
        $this->speakers = $speakers;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.speakers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.speakers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $this->speakers->create($request->all());

        return redirect()->route('dashboard.speakers.index')->withSuccess('Speaker Added Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Speaker $speaker)
    {
        return view('dashboard.speakers.edit', compact('speaker'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $this->speakers->update($request->all(), $id);

        return redirect()->route('dashboard.speakers.index')->withSuccess('Speaker Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $speaker = $this->speakers->with('events')->find($id);
        
        if ($speaker->events->count() > 0) {
            return redirect()->route('dashboard.speakers.index')->withWarning('Cannot delete this speaker! Delete his/her events first');
        }

        $this->speakers->delete($id);

        return redirect()->route('dashboard.speakers.index')->withSuccess(' Speaker Deleted Successfully');
    }
}
