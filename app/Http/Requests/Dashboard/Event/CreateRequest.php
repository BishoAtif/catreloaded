<?php

namespace App\Http\Requests\Dashboard\Event;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'   => 'required|unique:events',
            'title'  => 'required|string',
            'registration_form' => 'nullable|url',
            'url' => 'nullable|url',
            'image' => 'required|image',
            'speakers' => 'required|array',
            'speakers.*' => 'integer|exists:speakers,id',
        ];
    }
}
