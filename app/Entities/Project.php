<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['project', 'url', 'member_id'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}
