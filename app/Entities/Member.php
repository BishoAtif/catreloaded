<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Czim\Paperclip\Contracts\AttachableInterface;
use Czim\Paperclip\Model\PaperclipTrait;

class Member extends Model implements AttachableInterface
{
    use PaperclipTrait;

    protected $fillable = ['name', 'slug', 'title', 'subtitle', 'gold', 'facebook', 'twitter', 'linkedin', 'github', 'avatar', 'joined_at'];

    protected $hidden = ['avatar_file_name', 'avatar_file_size', 'avatar_content_type', 'avatar_updated_at'];

    protected $dates = ['joined_at'];

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('avatar',  [
            'variants' => [
                'web' => '300x300!',
                'thumb' => '100x100',
            ]
        ]);
        parent::__construct($attributes);
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'member_id');
    }

    public function skills()
    {
        return $this->hasMany(Skill::class, 'member_id');
    }

    public function experiences()
    {
        return $this->hasMany(Experience::class, 'member_id');
    }

    public function scopeCurrent($query)
    {
        return $query->where('gold', false);
    }

    public function scopeGold($query)
    {
        return $query->where('gold', true);
    }

}
