<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = ['skill', 'member_id'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}
