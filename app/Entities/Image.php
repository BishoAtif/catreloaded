<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Czim\Paperclip\Contracts\AttachableInterface;
use Czim\Paperclip\Model\PaperclipTrait;

class Image extends Model implements AttachableInterface
{
    use PaperclipTrait;

    protected $fillable = ['description', 'image'];

    protected $hidden = ['image_file_name', 'image_file_size', 'image_content_type', 'image_updated_at'];

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('image',  [
            'variants' => [
                'thumb' => '100x100',
                'web-slider' => '1360x370!',
            ],
        ]);
        parent::__construct($attributes);
    }

    public function configs()
    {
        return $this->hasMany(Config::class, 'image_id');
    }
}