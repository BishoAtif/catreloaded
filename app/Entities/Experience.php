<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $fillable = ['experience', 'duration', 'member_id'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}
