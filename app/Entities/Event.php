<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Czim\Paperclip\Contracts\AttachableInterface;
use Czim\Paperclip\Model\PaperclipTrait;
use Carbon\Carbon;

class Event extends Model implements AttachableInterface
{
    use PaperclipTrait;

    protected $fillable = ['title', 'slug' ,'url' ,'content', 'description', 'image', 'slider', 'date', 'registration_form'];

    protected $hidden = ['image_file_name', 'image_file_size', 'image_content_type', 'image_updated_at', 'slider_file_name', 'slider_file_size', 'slider_content_type', 'slider_updated_at'];

    protected $dates = ['date', 'created_at', 'updated_at'];

    public function __construct(array $attributes = array())
    {
        // TODO: Image Size
        $this->hasAttachedFile('image', [
            'variants' => [
                'web' => '600x600!',
                'thumb' => '80x120!',
            ],
        ]);

        $this->hasAttachedFile('slider', [
            'variants' => [
                'web' => '550x200!',
                'thumb' => '120x80!',
            ],
        ]);

        parent::__construct($attributes);
    }

    public function speakers()
    {
        return $this->belongsToMany(Speaker::class);
    }
}
