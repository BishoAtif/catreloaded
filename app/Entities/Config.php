<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = ['key', 'value', 'image_id'];

    public function image()
    {
        return $this->belongsTo(Image::class);
    }
}
