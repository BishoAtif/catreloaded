<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Czim\Paperclip\Contracts\AttachableInterface;
use Czim\Paperclip\Model\PaperclipTrait;

class Speaker extends Model implements AttachableInterface
{
    use PaperclipTrait;

    protected $fillable = ['name', 'bio', 'avatar', 'linkedin'];

    protected $hidden = ['avatar_file_name', 'avatar_file_size', 'avatar_content_type', 'avatar_updated_at'];

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('avatar', [
            'variants' => [
                'web' => '400x600',
                'thumb' => '80x100!',
            ]
        ]);

        parent::__construct($attributes);
    }

    public function events()
    {
        return $this->belongsToMany(Event::class);
    }

    public function sessions()
    {
        return $this->hasMany(Session::class);
    }
}
