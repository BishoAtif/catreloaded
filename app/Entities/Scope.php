<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Czim\Paperclip\Contracts\AttachableInterface;
use Czim\Paperclip\Model\PaperclipTrait;

class Scope extends Model implements AttachableInterface
{
    use PaperclipTrait;

    protected $fillable = ['title', 'slug', 'description', 'image', 'schedule', 'date', 'map', 'url'];

    protected $hidden = ['image_file_name', 'image_file_size', 'image_content_type', 'image_updated_at', 'schedule_file_name', 'schedule_file_size', 'schedule_content_type', 'schedule_updated_at'];

    protected $dates = ['date'];

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('image', [
            'styles' => [
                'medium' => '400x600',
                'thumb' => '80x120'
            ]
        ]);

        $this->hasAttachedFile('schedule', [
            'styles' => [
                'medium' => '400x600',
                'thumb' => '80x120'
            ]
        ]);

        parent::__construct($attributes);
    }
    
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }
}
