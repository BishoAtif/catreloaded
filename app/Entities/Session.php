<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = ['title', 'scope_id', 'speaker_id'];

    public function speaker()
    {
        return $this->belongsTo(Speaker::class);
    }

    public function scope()
    {
        return $this->belongsTo(Scope::class);
    }
}
