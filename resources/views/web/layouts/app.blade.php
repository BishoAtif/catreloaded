<!DOCTYPE html>
<html class="no-js" lang="en-GB">
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title') | CAT Reloaded</title>
    <meta http-equiv="Content-Type" content="text/html">
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/bootstrap.min.css') }}" charset="utf-8"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" type="text/css" charset="utf-8"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animations/2.1/css/animations.min.css" rel="stylesheet" type="text/css" charset="utf-8"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.min.css">
    <link href="{{ asset('web/css/responsive.css') }}" rel="stylesheet" type="text/css" charset="utf-8"/>
    <link rel='stylesheet' href="{{ asset('web/css/main.css') }}" type='text/css'/>
  </head>
  
  <body class="page-template page-template-about-cat page-template-about-cat-php page page-id-10 sidebar-primary">
    <div id="social">
      <ul class="list-unstyled">
        <li><a href="https://www.facebook.com/CATReloaded" target = "_blank"><img src="{{ asset('web/img/facebook2.png') }}" id="facebook"></a></li>
        <li><a href="https://twitter.com/CATreloaded" target = "_blank"><img src="{{ asset('web/img/twitter2.png') }}" id="twitter"></a></li>
        <li><a href="https://www.youtube.com/channel/UCJaNoxBopnEOli2mI7PTlgg" target = "_blank"><img src="{{ asset('web/img/youtube2.png') }}" id="youtube"></a></li>
      </ul>
    </div>
    <!--[if lt IE 8]>
      <div class="alert alert-warning">
        You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.    </div>
    <![endif]-->
    <header class="header">
      <nav class="navbar navbar-inverse" id="main-navbar" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <a href="{{ config('app.url') }}" class="navbar-brand col-xs-10 col-lg-12" id="logo"></a>
          </div>
          <div class="collapse navbar-collapse" id="nav_collapse">
            <ul id="nav" class="nav navbar-nav navbar-left">
              <li class="menu {{ App\Helpers\GeneralHelper::setActiveMenu(route('web.home')) }}"><a href="{{ route('web.home') }}">الرئيسية</a></li>
              <li class="menu {{ App\Helpers\GeneralHelper::setActiveMenu(route('web.about-us')) }}"><a href="{{ route('web.about-us') }}">عنّا</a></li>
              {{-- <li class="menu {{ App\Helpers\GeneralHelper::setActiveMenu(route('web.circles')) }}"><a href="{{ route('web.circles') }}">دوائر كات</a></li> --}}
              {{-- <li class="menu {{ App\Helpers\GeneralHelper::setActiveMenu(route('web.scopes.index')) }}"><a href="{{ route('web.scopes.index') }}">الأحداث</a></li> --}}
              <li class="menu {{ App\Helpers\GeneralHelper::setActiveMenu(route('web.events.index')) }}"><a href="{{ route('web.events.index') }}">تعلّم معنا</a></li>
              {{-- <li class="menu {{ App\Helpers\GeneralHelper::setActiveMenu(route('web.calender')) }}"><a href="{{ route('web.calender') }}">تقويم كات</a></li>
              <li class="menu {{ App\Helpers\GeneralHelper::setActiveMenu(route('web.contact-us')) }}"><a href="{{ route('web.contact-us') }}">اتصل بنا</a></li> --}}
            </ul>
          </div>
        </div>
       </nav>
    </header>

    @yield('content')

    <footer>
      <div class="container">
        <div class="row">
          <div class="col-xs-6 col-sm-4 col-sm-offset-0 col-xs-offset-3 footer-content">
            <div id="hangouts">
              <a href="https://www.youtube.com/c/CATHangouts" target="_blank" class="">
                <img src="{{ asset('web/img/CAT-hangouts.png') }}" class="center-block">
              </a>
              <p class="footer_labels text-center"> الهانج اوت الأسبوعي للكات</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-4 col-sm-offset-0 col-xs-offset-3 footer-content">
            <div id="catavee">
              <a href="https://www.youtube.com/user/CATaVee/feed" target="_blank" class="">
                <img src="{{ asset('web/img/catatv.png') }}" class="center-block">
              </a>
              <p class="footer_labels text-center">قناة الكات الرسمية</p>
            </div>
          </div>

            <div class="col-xs-6 col-sm-4 col-sm-offset-0 col-xs-offset-3 footer-content">
              <div id="catazine">
                <a href="https://blog.catreloaded.org/" target="_blank" class="">
                  <img src="{{ asset('web/img/catazine.png') }}" class="center-block">
                </a>
                <p class="footer_labels text-center">المجلة الالكترونية للكات</p>
              </div>
           </div>
        </div>
        <p class="copyright">CATReloaded.org <i class="fa fa-copyright"></i> {{ date('Y') }}</p>
      </div>
    </footer>
  </body>
  <script type='text/javascript' src="{{ asset('web/js/jquery.min.js') }}"></script>
  <script type='text/javascript' src="{{ asset('web/js/bootstrap.min.js') }}"></script>
  <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>
</html>
