@extends('web.layouts.app')

@section('title')
  About Us
@endsection

@section('content')
  <div id="about-header" class="header">
    <div class="container">
      <div id="about-des" class="text-right jumbotron header-container">
        <h1 class="text-center white-title">عن كات</h1>
        <p class="text-center white-p">الكات فريق يتجه الى أن يكون مؤسسة تتركز اهتماماته على علوم الكمبيوتر و ثقافة المصادر المفتوحة. منشأ الفريق الأول هو كلية الهندسة - جامعة المنصورة - مصر</p>
      </div>
    </div>
  </div>

  <div id="about_content">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h1 class="text-center red-title">هدفنا</h1>
          <p class="text-center"> خلق مجتمع معلوماتي قوي من المهندسين يرتقي بمستوي هندسة الحاسبات و كل المتعلق بها لمستوى ينافس و يفوق دول العالم الأول ومساعدة مصر للنهوض بالجهود الذاتية لتصبح من الدول المصنعة لتكنولوجيا المعلومات فى العالم.</p>
          <p class="text-center">يتجه مجتمع الكات فى الفترة الأخيرة الى ان يخدم المجتمع وان يجعل المجتمع يخدمه لتحقيق أهدافه, عن طريق نشر التعليم المتقدم و الابتدائى.</p>
          <h3 class="text-center">يتم ذلك عن طريق:</h3>
          <ul class="list-unstyled text-center">
            <li>مشاريع مفتوحة المصدر لها صدى على مستوى العالم</li>
            <li>دورات تدريبية</li>
            <li>مسابقات تقنية</li>
            <li>مساعدة فى التوظيف</li>
            <li>علاقات مع الشركات المحلية و الدولية للتمويل</li>
            <li>مؤتمرات على مستوى الوطن العربى و العالم, وفى أماكن مختلفة</li>
          </ul>
          <p class="text-center">فى الجانب الإنسانى, تعمل الكات من خلال استخدام التكنولوجيا على المساعدة فى إصلاح بعض المشاكل الموجودة فى مصر, سواء كانت على المدى القصير أو البعيد</p>
        </div>
      </div>
    </div>
  </div>

  <div class="header">
    <div class="container team">
      <h1 class="text-center red-title">أعضاء الفريق الحالي</h1>
      {{-- TODO Check the members allignment in xs screen, also check gold members --}}
      @foreach($members as $member)
        @if($loop->index % 4 == 0)
          <ul class="row">
        @endif
        <li class="col-md-3 col-sm-6 team-memeber">
          <ul class="list-unstyled">
            <li><img src="{{ $member->avatar->url('web') }}" class="img-responsive"/></li>
            {{-- TODO: Member Profile --}}
            {{-- <li><a href="{{ route('web.members.show', ['slug' => $member->slug]) }}"> <img src="{{ $member->avatar->url('web') }}" class="img-responsive"/></a></li> --}}
            <li class="member-name text-primary">{{ $member->name }}</li>
            {{-- <li class="member-name"><a href="{{ route('web.members.show', ['slug' => $member->slug]) }}">{{ $member->name }}</a></li> --}}
            <li class="member-hr"><hr></li>
            <li>
              <ul class="list-inline list-unstyled member-social">
                @if(!is_null($member->facebook))
                  <li><a href="{{$member->facebook}}" target ="_blank"><i class="fa fa-facebook fa-2x"></i></a></li>
                @endif
                @if(!is_null($member->github))
                  <li><a href="{{$member->github}}" target="_blank"><i class="fa fa-github-alt fa-2x"></i></a></li>
                @endif
                @if(!is_null($member->linkedin))
                  <li><a href="{{$member->linkedin}}" target="_blank"><i class="fa fa-linkedin-square fa-2x"></i></a></li>
                @endif
                @if(!is_null($member->twitter))
                  <li><a href="{{$member->twitter}}" target="_blank"><i class="fa fa-twitter fa-2x"></i></a></li>
                @endif
                @if(!is_null($member->google))
                  <li><a href="{{$member->google}}" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a></li>
                @endif
              </ul>
            </li>
          </ul>
        </li>      
        @if($loop->index % 4 == 3 || $loop->last)
          </ul>
        @endif
      @endforeach
      <div class="left-btn center-btn">
        <a href="{{ route('web.gold-members') }}" class="btn-default index-more">أعضاء الفريق القدامى</a>
      </div>
    </div>
  </div>
@endsection

