@extends('web.layouts.app')

@section('title')
  Circles
@endsection

@section('content')
  <div id="circle-header" class="header">
    <div class="container">
      <div id="circle" class="text-right jumbotron header-container">
        <div class="row">
          <img src="{{ asset('web/img/circles.png') }}" class="col-lg-2 col-sm-3 col-xs-6 col-sm-offset-0 col-xs-offset-3 img-responsive">
          <div id="circle_des" class="col-lg-10 col-sm-9 col-xs-12 header-des">
            <h1 class="white-title">دوائر كات</h1>
            <p>عبارة عن فرق فرعية تحت إدارة الكات سُميت CAT Circles مقسمة على حسب التخصصات للإنضمام لهذة المجموعات لا يشترط إطلاقا أن تكون عضواً فعالاً في الفريق أو مُسجلاً في القائمة البريدية الخاصة بالفريق أو تمتلك بطاقة عضوية أو أن تتواجد بصفة مستمرة .. الخ ، بل العكس تماما ًفالهدف من هذا الأمر هو "التعليم فقط" أو بالأحرى إثراء العلم ، ولكن بالشكل الذي يضمن أن يصل للناس بالشكل الصحيح!</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="circle_content">
    <div class="container">
      <div class="col-sm-1 col-sm-offset-9">
        <div class="pull-left" id="line1"></div>
      </div>
      <div class="circle col-xs-12">
        <div class="row">
          <img src="{{ asset('web/img/circles1.png') }}" class="pull-left col-lg-5 col-md-4 col-sm-5 col-xs-12 img-responsive circle-img">
          <div class="circle-content col-sm-7 col-xs-12">
            <div class="circle-des-right">
              <h2 class="circle-title">دائرة المصادر المفتوحة </h2>
              <p class="circle-des">هدفنا الأساسي من الدائرة هو إستخدام ونشر البرمجيات مفتوحة المصدر كثقافة عاملة و تطوير الأفراد فى مجالات إدارة الـ servers القائمة علي نظام تشغيل linux ونشر نظام التشغيل بين الأفراد و فى الشركات ،أيضا نتابع التكنولوجيا الحديثة وكل ما يخص برمجيات مفتوحة المصدر عموماً.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="animated line col-sm-1 col-sm-offset-4 line-container" id="line2-container">
        <div class="right-line" id="line2"></div>
      </div>
    </div>
    <div class="" id="circle2-bg">
      <div class="container">
        <div id="circle2" class="circle col-xs-12">
          <div class="row">
            <img src="{{ asset('web/img/circles2.png') }}" class="col-lg-5 col-md-4 col-sm-5 col-xs-12 img-responsive circle-img">
            <div class="circle-content col-sm-7 col-xs-12">
              <div class="circle-des-left">
                <h2 class="circle-title">دائرة الويب</h2>
                <p class="circle-des">تهدف دائرة الويب إلى تعلم مراحل تصميم وتطوير صفحات مواقع الإنترنت من البداية وحتى الوصول إلي إستخدام الـ frameworks الحديثة أيضا نتابع فيها آخر أخبار وأهم نصائح عالم الويب.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="animated line col-sm-1 col-sm-offset-4 line-container" id="line3-container">
          <div class="left-line" id="line3"></div>
        </div>
      </div>
    </div>
    <div class="container">
      <div id="circle3" class="circle col-xs-12">
        <div class="row">
          <img src="{{ asset('web/img/circles3.png') }}" class="pull-left col-lg-5 col-md-4 col-sm-5 col-xs-12 img-responsive circle-img">
          <div class="circle-content col-sm-7 col-xs-12">
            <div class="circle-des-right">
              <h2 class="circle-title">دائرة البرمجة</h2>
              <p class="circle-des">تهتم دائرة البرمجة بالتركيز على مبادئ وأساسيات البرمجة بوجه عام دون التركيز على لغة برمجة بعينها، كما تهتم بدراسة الخوارزميات (Algorithms) وأنمطة التصميم (Design Patterns)، وتتعرض الدائرة للغات برمجة مختلفة الأنماط (Programming paradigms).</p>
            </div>
          </div>
        </div>
      </div>
      <div class="animated line col-sm-1 col-sm-offset-4 line-container" id="line4-container">
        <div class="right-line" id="line4"></div>
      </div>
    </div>
    <div class="container">
      <div id="circle4" class="circle col-xs-12">
        <div class="row">
          <img src="{{ asset('web/img/circles4.png') }}" class="col-lg-5 col-md-4 col-sm-5 col-xs-12 img-responsive circle-img">
          <div class="circle-content col-sm-7 col-xs-12">
            <div class="circle-des-left">
              <h2 class="circle-title">دائرة الجرافيكس</h2>
              <p class="circle-des">تركز دائره الجرافيك علي قواعد التصميم بشكل عام وليس علي الادوات المستخدمه للتصميم مثل الـ photoshop او الـ Illustrator.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="animated line col-sm-1 col-sm-offset-4 line-container" id="line5-container">
        <div class="left-line" id="line5"></div>
      </div>
    </div>
    <div class="" id="circle5-bg">
      <div class="container">
        <div id="circle5" class="circle col-xs-12">
          <div class="row">
            <img src="{{ asset('web/img/circles5.png') }}" class="pull-left col-lg-5 col-md-4 col-sm-5 col-xs-12 img-responsive circle-img">
            <div class="circle-content col-sm-7 col-xs-12">
              <div class="circle-des-right">
                <h2 class="circle-title">دائرة برمجة الالعاب </h2>
                <p class="circle-des white-p">كواحده من أكثر المجالات سريعه التطور ، نحن فى فريق الـكات بدأنا الاهتمام بها و دمجها مع الأنشطة الحاليه . ستهتم دائرة الـ Game بالمفاهيم والأدوات المستخدمه في صناعه الألعاب لمختلفه المنصات ،و سيشارك الفريق أيضا في تعلم وتدريس وعمل مشاريع جديده في مجالات أخرى مرتبطه كالـ Virtual Reality و Augmented Reality و 3D Works.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="animated line col-sm-1 col-sm-offset-4 line-container" id="line6-container">
          <div class="right-line" id="line6"></div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div id="circle6" class="circle">
            <img src="{{ asset('web/img/circles6.png') }}" class="col-lg-5 col-md-4 col-sm-5 col-xs-12 img-responsive circle-img">
            <div class="circle-content col-sm-7 col-xs-12">
              <div class="circle-des-left">
                <h2 class="circle-title">دائرة تطوير الموبايل </h2>
                <p class="circle-des">تهتم دائرة الموبايل بالتركيز على كل جديد في مجال الـ Android و الـ IOS ، ونعمل معًا على تصميم وتطوير بعض التطبيقات والمشاريع الداخلية المتعلقة بالفريق.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="animated line col-sm-1 col-sm-offset-4 line-container" id="line7-container">
        <div class="left-line" id="line7"></div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div id="circle7" class="circle">
            <img src="{{ asset('web/img/circles7.png') }}" class="pull-left col-lg-5 col-md-4 col-sm-5 col-xs-12 img-responsive circle-img">
            <div class="circle-content col-sm-7 col-xs-12">
              <div class="circle-des-right">
                <h2 class="circle-title">دائرة التحكم </h2>
                <p class="circle-des">تختص الدائرة بتقديم كل ما تحتاجه في طريقك نحو أنظمة التحكم ، بداية من تصميمها واختيار مكوناتها المادية و البرمجية المناسبة وحتى تمثيلها واختبارها و تقديمها في الأشكال المختلفة من مشاريع الروبوتات و أنظمة الحماية أو التشغيل. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="animated line col-sm-1 col-sm-offset-4 line-container" id="line8-container">
        <div class="right-line" id="line8"></div>
      </div>
    </div>
    <div class="" id="circle8-bg">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div id="circle8" class="circle">
              <div class="col-lg-5 col-md-4 col-sm-5 col-xs-12">
                <img src="{{ asset('web/img/circles8.png') }}" class="img-responsive circle-img">
              </div>
              <div class="col-sm-7 col-xs-12">
                <div class="circle-content">
                  <div class="circle-des-left">
                    <h2 class="circle-title">دائرة الحماية </h2>
                    <p class="circle-des white-p">تهدف دائرة الـ security إلي تعلم أساسيات أمن المعلومات والشبكات وأبرز أنواع الهجمات والثغرات وأيضا الإطلاع علي كل ما هو جديد من أخبار الأمن الإلكتروني وأحدث الثغرات وبرامج الحماية.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
