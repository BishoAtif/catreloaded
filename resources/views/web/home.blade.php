@extends('web.layouts.app')

@section('title')
  Home
@endsection

@section('content')
  <div class="carousel slide" id="gallery-carousel" data-ride="carousel">
    <div class="carousel-inner">
      @foreach ($sliderImages as $sliderImage)
        <div class="item @if($loop->first) active @endif">
          <a href="{{ $sliderImage->value }}">
            <img src="{{ $sliderImage->image->image->url('web-slider') }}" alt="{{ $sliderImage->image->description }}" class="center-block">
          </a>
        </div>  
      @endforeach
    </div>
    @if($sliderImages->count() > 1)
      <a href="#gallery-carousel" class="left carousel-control" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a href="#gallery-carousel" class="right carousel-control" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
    @endif
  </div>
  <div class="container index-block">
    <div class="row">
      <div class="col-md-4 col-sm-3">
        <h1 class="latest-news-title">آخر الأخبار</h1>
      </div>
      <div class="col-md-4 col-sm-5 col-sm-offset-4 col-md-offset-4">
        <div class="search-div">
          {{-- @include('web.layouts._search_box') --}}
          {{-- TODO: Fix searchbox --}}
        </div>
      </div>
      <div class="col-xs-12">
        <ul class="list-unstyled list-inline news-block">
          @foreach($recentEvents as $event)
          <li class="col-md-6">
            <a href={{ route('web.events.show', [ 'slug'=> $event->slug]) }}/><img src="{{ $event->slider->url('web') }}" class="img-responsive"></a>
            <h3><a href="{{ route('web.events.show', ['slug' => $event->slug]) }}">{{ $event->title }}</a></h3>
            <p>{{ $event->date->diffForHumans() }}</p>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
  <div class="header">
    <div class="container index-block" id="circle_des">
      <a href="{{ route('web.circles') }}">
        <h1 class="text-center white-title">دوائر الكات | CAT Circles</h1>
      </a>
      <p class="text-center">عبارة عن فرق فرعية تحت إدارة الكات سُميت CAT Circles مقسمة على حسب التخصصات للإنضمام لهذة المجموعات لا يشترط إطلاقا أن تكون عضواً فعالاً في الفريق ، مُسجلاً في القائمة البريدية الخاصة بالفريق ، تمتلك بطاقة عضوية أو أن تتواجد بصفة مستمرة .. الخ ، بل العكس تماما ًفالهدف من هذا الأمر هو "التعليم فقط" أو بالأحرى إثراء العلم ، ولكن بالشكل الذي يضمن أن يصل للناس بالشكل الصحيح !</p>
      <ul class="list-inline list-unstyled index-circle row">
        <li class="col-md-1 col-sm-2 col-xs-6">
          <img src="{{ asset('web/img/1.png') }}" class="img-responsive center-block">
        </li>
        <li class="col-md-1 col-sm-2 col-xs-6">
          <img src="{{ asset('web/img/2.png') }}" class="img-responsive center-block">
        </li>
        <li class="col-md-1 col-sm-2 col-xs-6">
          <img src="{{ asset('web/img/3.png') }}" class="img-responsive center-block">
        </li>
        <li class="col-md-1 col-sm-2 col-xs-6">
          <img src="{{ asset('web/img/4.png') }}" class="img-responsive center-block">
        </li>
        <li class="col-md-1 col-sm-2 col-xs-6">
          <img src="{{ asset('web/img/5.png') }}" class="img-responsive center-block">
        </li>
        <li class="col-md-1 col-sm-2 col-xs-6">
          <img src="{{ asset('web/img/6.png') }}" class="img-responsive center-block">
        </li>
        <li class="col-md-1 col-sm-2 col-xs-6">
          <img src="{{ asset('web/img/7.png') }}" class="img-responsive center-block">
        </li>
        <li class="col-md-1 col-sm-2 col-xs-6">
          <img src="{{ asset('web/img/8.png') }}" class="img-responsive center-block">
        </li>
      </ul>
    </div>
  </div>
  <div class="container index-block">
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-8 col-md-offset-0 col-sm-offset-3 col-xs-offset-2 index-img">
        <img src="{{ asset('web/img/catazine.png') }}" class="img-responsive center-block">
      </div>
      <div class="col-md-8 col-xs-12">
        <h1 class="center-h1">كاتازين | CATazine</h1>
        <p>اﻟﻤﺠﻠﺔ اﻟﺮﺳﻤﻴﺔ ﻟﻔﺮﻳﻖ CAT. تأسست ﻋﺎﻡ 2008 ﻭﺑﺪأ اﻟﻨﺸﺮ ﻓﻲ ﻧﻔﺲ اﻟﻌﺎﻡ ﻟﻴﺘﻢ الإﻋﻼﻥ ﺭسمياً ﻋﻦ إﻧﻄﻼﻕ أﻭﻝ ﻋﺪﺩ ﻟﻤﺠﻠﺔ ” ﻛﺎﺗﺎﺯﻳﻦ “.اﻟﻜﺎﺗﺎﺯﻳﻦ ﻣﺠﻠﺔ ﻋﺮﺑﻴﺔ ﺩﻭﺭﻳﺔ متخصصة ﻓﻲ مجال ﺗﻜﻨﻮﻟﻮﺟﻴﺎ اﻟﻤﻌﻠﻮﻣﺎﺕ، تضم العديد من المقالات المتنوعة فى المجال. يشارك فيها اﻟﻤﺒﺎﺩﺭﻳﻦ ﺑﻤﻌﺮﻓﺘﻬﻢ ﻭﺧﺒﺮاﺗﻬﻢ وإن كانت بسيطة جدًا. يمكنك مشاركتنا مقالك للمزيد من التفاصيل اطلع على موقع الكاتزين.</p>
        <div class="left-btn center-btn">
          <a href="https://blog.catreloaded.org/" class="btn-default index-more">تصفح الكاتازين من هنا</a>
        </div>
      </div>
    </div>
  </div>
  <div class="header">
    <div class="container index-block">
      <h1 class="text-center white-title">كات هانج أوت | CAT Hangout </h1>
      <p class="text-center hangout-des">بث للنقاشات والحوارات التقنية التي يقدمها نخبة من المهندسين والعاملين في مجال تكنولوجيا المعلومات.</p>
      <ul class="row list-inline list-unstyled">
        <li class="col-sm-4 col-xs-8 col-sm-offset-0 col-xs-offset-2">
          <img src="{{ asset('web/img/hangout.png') }}" class="img-responsive center-block index-hangout">
          <p class="text-center hangout-des2">قُم بتقديم محتوىً جيّد</a>
        </li>
        <li class="col-sm-4 col-xs-8 col-sm-offset-0 col-xs-offset-2">
          <img src="{{ asset('web/img/speaker.png') }}" class="img-responsive center-block index-speaker">
          <p class="text-center hangout-des2">تَواصل مع المهتمين</a>
        </li>
        <li class="col-sm-4 col-xs-8 col-sm-offset-0 col-xs-offset-2">
          <img src="{{ asset('web/img/people.png') }}" class="img-responsive center-block index-people">
          <p class="text-center hangout-des2">سجِّل لإلقاء محاضرة</a>
        </li>
      </ul>
      <div class="text-center hangout-btn">
        <div class="view-hangout" ‎><a href="https://www.youtube.com/user/CATHangouts/videos" target="_blank" class="btn-default index-more">شاهد CAT Hangoust</a></div>
      </div>
    </div>
  </div>
  <div class="container index-block">
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-8 col-md-offset-0 col-sm-offset-3 col-xs-offset-2">
        <img src="{{ asset('web/img/catatv.png') }}" class="img-responsive center-block">
      </div>
      <div class="col-md-8 col-xs-12">
        <h1 class="center-h1 index-catavee">كاتافي | CATavee</h1>
        <p class="pull-left ">أحد أهم مقومات الشخصيات الطموحة هما المثل الأعلي والسعي وراء اكتساب الخبره. فى كاتافي ننتقي أفضل الشخصيات فى المجالات المختلفة فى مجتمعنا التقني ونسعي لعرض تجاربهم الشخصية فى مجال الحاسب ، أيضاً نلتمس منهم النصائح والتوصيات لنضع البعض علي أول الطريق. يتم عرض المقابلات بشكل شهري على القناة الخاصة بنا علي موقع اليوتيوب. ويقف وراء المقابلات مجموعه من أعضاء الفريق.</p>
        <div class="right-btn center-btn">
          <a href="https://www.youtube.com/user/CATaVee/videos" target="_blank" class="btn-default index-more">شاهد CATavee من هنا</a>
        </div>
      </div>
    </div>
  </div>
  <div class="header">
    <div class="container index-block">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-8 col-md-offset-0 col-sm-offset-3 col-xs-offset-2 index-img">
          <img src="{{ asset('web/img/library2.png') }}" class="img-responsive center-block cat-library-img">
        </div>
        <div class="col-md-8 col-sm-12">
          <h1 class="white-title center-h1">مكتبة الكات | CAT Library</h1>
          <p class="library-des">CAT Library نشاط جديد من أنشطة CAT Reloaded في محاولة للمساعدة في نشر العلم، عن طريق تسهيل استعارة أشهر وأفضل الكتب في مجال هندسة وعلوم الكمبيوتر، بمبالغ رمزية. انتظرنا مع تجديد مستمر لقائمة الكتب.</p>
        </div>
      </div>
    </div>
  </div>
@endsection
