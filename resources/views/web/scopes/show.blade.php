@extends('web.layouts.app')

@section('title')
  {{ $scope->title }}
@endsection

@section('content')
  <div class="header" >
    <div class="container">
      <div class="text-right jumbotron header-container session-header">
        <div class="row">
          <div class="col-sm-5 col-xs-12">
            <img src="{{ $scope->image->url() }}" class="img-responsive center-block">
          </div>
          <div class="col-sm-7 col-xs-12">
            <div class="header-des">
              <h1 class="white-title">{{ $scope->title }}</h1>
              <p>{{ $scope->description }}</p>
            </div>
          </div>
        </div>
        @if($scope->url)<a href="{{ $scope->url }}" id="event-inner_more" target = "_blank" class="pull-left btn-default">الحدث على فيسبوك</a>@endif
      </div>
    </div>
  </div>
  <div id="scope-inner_content">
    @if($scope->schedule)
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div id="schedule">
              <h1 class="red-title">الجدول</h1>
              <img src="{{ $scope->schedule->url() }}" class="img-responsive center-block">
            </div>
          </div>
        </div>
      </div>
    @endif

    @if(count($scope->sessions))
      <div class="header">
        <div class="container">
          <h1 class="red-title">المتحدثون</h1>
          <div class="row">
            @foreach($scope->sessions as $session)
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="speaker center-block">
                  <img src="{{ $session->speaker->avatar->url() }}" class="img-responsive img-circle center-block"></img>
                  <div class="speaker-des col-xs-12">
                    <p class="text-center">{{ $session->speaker->name }}</p>
                    <p class="text-center">{{ $session->title }}</p>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    @endif

    @if($scope->map)
      <div class="container" id="sponsor">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <iframe src="{{ $scope->map }}" width="100%" height="350" frameborder="0" style="border:0"></iframe>
          </div>
        </div>
      </div>
    @endif
@endsection
