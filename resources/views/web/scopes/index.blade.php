@extends('web.layouts.app')

@section('title')
  Scopes
@endsection

@section('content')
  <div  id="join_header" class="header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          @include('web.layouts._search_box')
          <div id="join_title" class="text-right jumbotron header-container">
            <h3 class="white-title">اختر ما يناسبك</h3>
            <h1 class="inline white-title">وانضم الآن&#33;</h1>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="join_content">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-xs-12">
          <div class="tab-content">
            <div class="tab-pane fade">
              <img src="http://catreloaded.org/wp-content/themes/cat/assets/img/no-posts.png" class="img-responsive">
            </div>
            @foreach($scopes as $scope)
              <div class="join row">
                <div class="col-sm-6 col-xs-12">
                  <a href="{{ route('web.scopes.show', ['slug' => $scope->slug]) }}" class="join_pic">
                    <img src="{{ $scope->image->url() }}" class="img-responsive">
                  </a>
                </div>
                <div class="col-sm-6 col-xs-12">
                  <div class="join_des">
                    <a href="{{ route('web.scopes.show', ['slug' => $scope->slug]) }}/">
                      <h2 dir="auto"><bdi>{{ $scope->title }}</bdi></h2>
                    </a>
                    <p dir="auto">{{ $scope->description }}</p>
                  </div>
                </div>
              </div>
            @endforeach
            {{ $scopes->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
