@extends('web.layouts.app')

@section('title')
  Gold Members
@endsection

@section('content')
  <div id="about-header" class="header">
    <div class="container">
      <div id="about-des" class="text-right jumbotron header-container">
        <h1 class="text-center white-title">عن كات</h1>
        <p class="text-center white-p">الكات فريق يتجه الى أن يكون مؤسسة تتركز اهتماماته على علوم الكمبيوتر و ثقافة المصادر المفتوحة. منشأ الفريق الأول هو كلية الهندسة-جامعة المنصورة-مصر.</p>
      </div>
    </div>
  </div>

  <div id="about_content">
    <div class="container team old-team">
      <div class="row">
        <h1 class="text-center red-title">أعضاء الفريق القدامى</h1>
        <ul class="list-inline list-unstyled">
          <div class="team-display">
            @foreach($members as $member)
              <li class="col-md-3 col-sm-4 col-xs-8 col-sm-offset-0 col-xs-offset-2 team-memeber">
                <ul class="list-unstyled">
                  <li><a href="{{ route('web.members.show', ['slug' => $member->slug]) }}"> <img src="{{ $member->avatar->url('medium') }}" class="img-responsive"/></a></li>
                  <li class="old-member-name"><a href="{{ route('web.members.show', ['slug' => $member->slug]) }}">{{ $member->name }}</a></li>
                  <li class="member-hr"><hr></li>
                  <li>
                    <ul class="list-inline list-unstyled member-social">
                      @if(!is_null($member->facebook))
                        <li><a href="{{$member->facebook}}" target ="_blank"><i class="fa fa-facebook fa-lg"></i></a></li>
                      @endif
                      @if(!is_null($member->github))
                        <li><a href="{{$member->github}}" target="_blank"><i class="fa fa-github-alt fa-lg"></i></a></li>
                      @endif
                      @if(!is_null($member->linkedin))
                        <li><a href="{{$member->linkedin}}" target="_blank"><i class="fa fa-linkedin-square fa-lg"></i></a></li>
                      @endif
                      @if(!is_null($member->twitter))
                        <li><a href="{{$member->twitter}}" target="_blank"><i class="fa fa-twitter fa-lg"></i></a></li>
                      @endif
                      @if(!is_null($member->google))
                        <li><a href="{{$member->google}}" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a></li>
                      @endif
                    </ul>
                  </li>
                </ul>
              </li>
            @endforeach
          </div>
        </ul>
      </div>
    </div>
  </div>
@endsection
