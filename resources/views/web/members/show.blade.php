@extends('web.layouts.app')

@section('title')
  {{ $member->name }}
@endsection

@section('content')
  <div id="profile-header" class="container">
    <img src="{{ $member->avatar->url('medium') }}" class="center-block">
    <h1 class="text-center">{{ $member->name }}</h1>
    <h3 class="text-center">{{ $member->title }}</h3>
    <h4 class="text-center">{{ $member->subtitle }}</h4>
    @if(!is_null($member->joined_at)) <p class="text-center">منذ {{ $member->joined_at->year }}</p> @endif
    <div class="text-center profile-social">
      @if(!is_null($member->facebook)) <a href="{{ $member->facebook }}" target="_blank"><i class="fa fa-facebook fa-lg"></i></a> @endif
      @if(!is_null($member->facebook)) <a href="{{ $member->google }}" target="_blank"><i class="fa fa-google-plus fa-lg"></i></i></a> @endif
      @if(!is_null($member->facebook)) <a href="{{ $member->twitter }}" target="_blank"><i class="fa fa-twitter fa-lg"></i></a> @endif
    </div>
  </div>

  <div id="profile-content" class="container">
    <div class="row">
      <div id="content">
        <div class="col-md-3 col-sm-4">
          <nav class="" id="myScrollspy">
            <ul class="nav nav-tabs nav-stacked xs-hidden" id="myNav">
              <li class="active"><a href="#section-1"><i class="fa fa-list"></i> المهارات</a></li>
              <li><a href="#section-2"><i class="fa fa-line-chart"></i> الخبرات</a></li>
              <li><a href="#section-3"><i class="fa fa-suitcase"></i> المشاريع</a></li>
            </ul>
          </nav>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
          <div class="member-left-col">
            <div id="section-1">
              <h2>المهارات</h2>
              <ul class="list-unstyled">
                @foreach($member->skills as $skill)
                  <li><i class="fa fa-circle"></i>{{ $skill->skill }}</bdi></li>
                @endforeach
              </ul>
            </div>
            <div id="section-2">
              <h2>الخبرات</h2>
              @foreach($member->experiences as $experience)
                <div class="sub-section">
                  <h3>{{$experience->experience}}</h3>
                </div>
              @endforeach
            </div>
            <div id="section-3">
              <h2>المشاريع</h2>
              @foreach($member->projects as $project)
                <div class="sub-section">
                  <h3>{{ $project->project }}</h3>
                  @if(!is_null($project->url)) <a href="{{ $project->url }}" target = "_blank"></a> @endif
                </div>
              @endforeach
            </div>
          </div>          
        </div>
      </div> 
    </div>
  </div>
@endsection