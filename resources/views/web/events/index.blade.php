@extends('web.layouts.app')

@section('title')
  Events
@endsection

@section('content')
  <div  id="join_header" class="header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          {{-- @include('web.layouts._search_box') --}}
          {{-- TODO: Fix Search --}}
          <div id="join_title" class="text-right jumbotron header-container">
            <h3 class="white-title">اختر ما يناسبك</h3>
            <h1 class="inline white-title">وانضم الآن&#33;</h1>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="join_content">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-xs-12">
          <ul class="nav nav-pills nav-justified list-inline event-pills" id="myTab">
            @if($upcomingEvents->count() > 0)<li class="active"><a href="#section-1" data-toggle="tab"><i class="active fa fa-sign-out"></i> قريباً</a></li>@endif
            <li class="@if($upcomingEvents->count() == 0)active @endif"><a href="#section-2" data-toggle="tab"><i class="fa fa-history"></i> أرشيف</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane fade in @if($upcomingEvents->count() > 0) active @endif" id="section-1">
              @foreach($upcomingEvents as $event)
                <div class="join row">
                  <div class="col-sm-6 col-xs-12">
                    <a href="{{ route('web.events.show', ['slug' => $event->slug]) }}" class="join_pic">
                      <img src="{{ $event->image->url('web') }}" class="img-responsive">
                    </a>
                  </div>
                  <div class="col-sm-6 col-xs-12">
                    <div class="join_des">
                      <a href="{{ route('web.events.show', ['slug' => $event->slug]) }}/">
                        <h2 dir="auto"><bdi>{{ $event->title }}</bdi></h2>
                      </a>
                      <p dir="auto">{{ $event->description }}</p>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
            <div class="tab-pane fade in @if($upcomingEvents->count() == 0) active @endif" id="section-2">
              @foreach($pastEvents as $event)
                <div class="join row">
                  <div class="col-sm-6 col-xs-12">
                    <a href="{{ route('web.events.show', ['slug' => $event->slug]) }}" class="join_pic">
                      <img src="{{ $event->image->url('web') }}" class="img-responsive">
                    </a>
                  </div>
                  <div class="col-sm-6 col-xs-12">
                    <div class="join_des">
                      <a href="{{ route('web.events.show', ['slug' => $event->slug]) }}/">
                        <h2 dir="auto"><bdi>{{ $event->title }}</bdi></h2>
                      </a>
                      <p dir="auto">{{ $event->description }}</p>
                    </div>
                  </div>
                </div>
              @endforeach
              {{ $pastEvents->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="http://catreloaded.org/wp-content/themes/cat/assets/js/bootstrap-tabcollapse.js"></script>
  <script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href");
    target = target.substring(1);
    localStorage.setItem("target", target);
    });

    var tab = localStorage.getItem("target");
    $(document).ready(function(){
      activaTab(tab);
    });

    function activaTab(tab){
      $('.nav-pills a[href="#' + tab + '"]').tab('show');
    };
  </script>
@endsection
