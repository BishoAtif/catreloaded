@extends('web.layouts.app')

@section('title')
  {{ $event->title }}
@endsection

@section('content')
  <div id="session-header" class="header" >
    <div class="container">
      <div class="text-right jumbotron header-container session-header">
        <div class="row">
          <div class="col-sm-5 col-xs-12">
            <img src="{{ $event->slider->url('web') }}" class="img-responsive center-block session-img">
          </div>
          <div class="col-sm-7 col-xs-12">
            <div class="header-des">
              <h1 class="white-title" dir="auto"><bdi>{{ $event->title }}</bdi></h1>
              <h3 dir="auto">{{ $event->date->diffForHumans() }}</h3>
              <p dir="auto">{{ $event->description }}</p>
            </div>
          </div>
        </div>
        @if($event->url)
          <a href="{{ $event->url }}" target = "_blank" id="event-inner_more" class="pull-left btn-default">الحدث على فيسبوك</a>
        @endif
        @if($event->registration_form)
          <a href="{{ $event->registration_form }}" target = "_blank" id="session-inner-reg" class="pull-left btn-default">التسجيل</a>
        @endif
      </div>
    </div>
  </div>
  <div class="session-inner-content">
    <div class="container">
      <div class="row">
      <div class="col-xs-12"><h1 class="red-title">@if(count($event->speakers) == 1) المتحدث @else المتحدثون @endif</h1>
        @foreach($event->speakers as $speaker)
          <div class="row speaker-info">
            <div class="col-sm-4">
              <img class="center-block img-circle session-speaker-img" src="{{ $speaker->avatar->url() }}"></img>
            </div>
            <div class="col-sm-8">
              <div class="speaker-info-text">
                <h1 class="session-speaker-name">{{ $speaker->name }}</h1>
                @if($speaker->linkedin)<a href="{{ $speaker->linkedin }}"><i class="fa fa-linkedin-square fa-2x"></i></a>@endif
                <p dir="auto" class="about-speaker">{!! $speaker->bio !!} <br /></p>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <div class="header">
    <div class="container session-more">
      <div class="row">
        <div class="col-xs-12">
          <h1 class="red-title">المزيد عن الحدث</h1>
          <div class="session-more-text">
            <p dir="auto">{!! $event->content !!}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
