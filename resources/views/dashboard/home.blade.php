@extends('dashboard.layouts.app')

@section('title')
  Home
@endsection

@section('content')
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4>
      <i class="icon fa fa-check"></i> Welcome to the dashboard, {{ auth()->user()->name }}! Hope you are doing well :)</h4>
  </div>

@endsection
