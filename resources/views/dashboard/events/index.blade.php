@extends('dashboard.layouts.app')

@section('title')
  Events
@stop

@section('styles')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
  <div class="box">
    <div class="box-body">
      <button type="button" class="btn bg-olive margin" onclick="window.location='{!! route('dashboard.events.create') !!}'">Add Event</button>
      <table id="events-table" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Title</th>
            <th>Actions</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
@endsection

@section('footerScripts') 
  <script>
    require(['jquery', 'datatables.net', 'dataTables'], function($, DataTable){
      $('#events-table').DataTable( {
        serverSide: true,
        processing: true,
        ajax: {
          url: '{!! route('dashboard.ajax.events.index') !!}'
        },
        columns: [
          { data: 'title' },
          {
            data: "actions",
            render: function(data){
              return htmlDecode(data);
             }
          }
        ]
      });
    });
  </script>
@endsection
