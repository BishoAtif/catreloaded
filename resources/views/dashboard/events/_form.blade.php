@section('styles')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
@endsection

<div class="row">
  <div class="col-lg-3">
    <div class="form-group @if($errors->first('title')) has-error @endif">
      <label class="control-label" for="title">Title</label>
      {{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title']) }}
      @if($errors->first('title')) <span class="help-block">{{ $errors->first('title') }}</span> @endif
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group @if($errors->first('slug')) has-error @endif">
      <label class="control-label" for="slug">Slug</label>
      {{ Form::text('slug', old('slug'), ['class' => 'form-control', 'id' => 'slug']) }}
      @if($errors->first('slug')) <span class="help-block">{{ $errors->first('slug') }}</span> @endif
    </div>
  </div>
</div>
  
<div class="row">
  <div class="col-lg-3">
    <div class="form-group @if($errors->first('date')) has-error @endif">
      <label class="control-label" for="date">Date</label>
      <div class="input-group date">
        {{ Form::text('date', old('date'), ['class' => 'form-control', 'id' => 'date']) }}
      </div>
      @if($errors->first('date'))<span class="help-block">{{ $errors->first('date') }}</span>@endif
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group @if($errors->first('url')) has-error @endif">
      <label class="control-label" for="url">Facebook Url</label>
      {{ Form::text('url', old('url'), ['class' => 'form-control', 'id' => 'url']) }}
      @if($errors->first('url')) <span class="help-block">{{ $errors->first('url') }}</span> @endif
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group @if($errors->first('registration_form')) has-error @endif">
      <label class="control-label" for="registration_form">Registration Form</label>
      {{ Form::text('registration_form', old('registration_form'), ['class' => 'form-control', 'id' => 'registration_form']) }}
      @if($errors->first('registration_form')) <span class="help-block">{{ $errors->first('registration_form') }}</span> @endif
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-3">
    <div class="form-group @if($errors->first('image')) has-error @endif">
      <label for="image" class="control-label">Image</label>
      @if(isset($event))
        <div class="row">
          <div class="col-lg-4">
            <img src="{{ $event->image->url('thumb') }}">
          </div>
        </div>
        <a href="{{ $event->image->url() }}" target="_blank">Get Original Image</a>
        {!! Form::file('image', ['class' => 'custom-file-input']) !!}
      @else
        {!! Form::file('image', ['class' => 'custom-file-input', 'required' => true]) !!}
      @endif
      @if($errors->first('image')) <span class="help-block">{{ $errors->first('image') }}</span> @endif
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group @if($errors->first('slider')) has-error @endif">
      <label for="slider" class="control-label">Slider</label>
      @if(isset($event))
        <div class="row">
          <div class="col-lg-4">
            <img src="{{ $event->slider->url('thumb') }}">
          </div>
        </div>
        <a href="{{ $event->slider->url() }}" target="_blank">Get Original Image</a>
        {!! Form::file('slider', ['class' => 'custom-file-input']) !!}
      @else
        {!! Form::file('slider', ['class' => 'custom-file-input', 'required' => true]) !!}
      @endif
      @if($errors->first('slider')) <span class="help-block">{{ $errors->first('slider') }}</span> @endif
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-6">
    <div class="form-group @if($errors->first('description')) has-error @endif">
      <label class="control-label" for="description">Short Description</label>
      {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'id' => 'description']) }}
      @if($errors->first('description'))<span class="help-block">{{ $errors->first('description') }}</span>@endif
      </div>
    </div>
</div>

<div class="row">
  <div class="col-lg-9">
    <div class="form-group @if($errors->first('content')) has-error @endif">
      <label class="control-label" for="content">Content</label>
      {{ Form::textarea('content', old('content'), ['class' => 'form-control', 'id' => 'content']) }}
      @if($errors->first('content'))<span class="help-block">{{ $errors->first('content') }}</span>@endif
      </div>
    </div>
</div>

<div class="repeated-inputs">
  <label class="control-label">Speakers</label>
  @if(isset($event) && $event->speakers->count() > 0)
    @if($errors->first('speakers.*'))
      <span class="help-block text-danger">{{ $errors->first('speakers.*') }}</span>
    @endif
    @foreach ($event->speakers as $speaker)
      <div class="row">
        <div class="col-sm-4 col-md-2">
          <div class="form-group @if($errors->first('id')) has-error @endif">
            {{ Form::select("speakers[]", [$speaker->id => $speaker->name], $speaker->id ,  ['placeholder' => '' , 'class' => 'form-control id', 'data-name-template' => "speakers[]"]) }}
          </div>
        </div>
        <div class="col-sm-4 col-md-2">
          <button type="button" class="btn btn-danger remove-input">Remove Speaker</button>
        </div>
      </div>
    @endforeach
  @else
    <label class="control-label">Speakers</label>
    <div class="row">
      <div class="col-sm-4 col-md-2">
        <div class="form-group @if($errors->first('id')) has-error @endif">
          {{ Form::select("speakers[]", [], [] ,  ['placeholder' => '' , 'class' => 'form-control', 'data-name-template' => "speakers[]"]) }}
        </div>
      </div>
      <div class="col-sm-4 col-md-3 ">
        <button type="button" class="btn btn-danger remove-input">Remove Speaker</button>
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col-sm-push-4 col-md-push-2 col-sm-4 col-md-3">
      <button type="button" class="btn btn-warning add-input">Add Speaker</button>
    </div>
  </div>  
</div>

@section('footerScripts')
  <script>
    var url = "{{ route('dashboard.ajax.speakers.search') }}";
    require(['jquery', 'datetimepicker', 'ckeditor'], function($){
      $('#date').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
      });
      CKEDITOR.replace('content', {
        language : 'ar'
      });
    });
  </script>
  <script src="{{ asset('dashboard/js/forms.js') }}"></script>
@endsection
