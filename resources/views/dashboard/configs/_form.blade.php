<h2>Slider Config</h2>
<div class="repeated-inputs">
  @foreach ($sliders as $key => $slider)
    <div class="row">
      <div class="col-sm-4 col-md-3">
        <div class="form-group @if($errors->first('link')) has-error @endif">
          <label class="control-label" for="link">Link</label>
          {{ Form::url("sliders[{$key}][link]", $slider->value, ['class' => 'form-control', 'id' => 'link', 'data-name-template' => "sliders[{}][link]"]) }}
        </div>
      </div>
      <div class="col-sm-4 col-md-3">
        <div class="form-group @if($errors->first('image')) has-error @endif">
          <label for="image" class="control-label">image</label>
          {{ Form::select("sliders[{$key}][image]", [$slider->image_id => @$slider->image->description], $slider->image_id ,  ['placeholder' => '' , 'class' => 'form-control image', 'data-name-template' => "sliders[{}][image]"]) }}
          @if($errors->first('image'))
            <span class="help-block">{{ $errors->first('image') }}</span>
          @endif
        </div>
      </div>
      <div class="col-sm-4 col-md-3">
        @if(isset($slider) && $slider->image)
          <div class="row">
            <div class="col-lg-3">
              <img src="{{ $slider->image->image->url('thumb') }}">
            </div>
          </div>
          <a href="{{ $slider->image->image->url() }}" target="_blank">Get Original Image</a>
        @endif
      </div>
      <div class="col-sm-4 col-md-3">
        <button type="button" class="btn btn-danger remove-input">Remove Slider</button>
      </div>
    </div>
  @endforeach
  <div class="row">
    <div class="col-sm-push-8 col-md-push-9 col-sm-4 col-md-3">
      <button type="button" class="btn btn-warning add-input">Add Slider</button>
    </div>
  </div>  
</div>