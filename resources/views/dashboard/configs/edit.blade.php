@extends('dashboard.layouts.app')

@section('title')
  Configs
@stop

@section('content')
  <div class="container">
    {{ Form::open(['route' => ['dashboard.configs.store'], 'method' => 'POST']) }}
      @include('dashboard.configs._form')
      <div class="form-actions">
        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
          <button type="button" class="btn btn-danger" onclick="window.location='{{ route('dashboard.home') }}';">Back</button>
        </div>
      </div>
    {{ Form::close() }}
  </div>
@endsection

@section('footerScripts')
  <script>
    var url = "{{ route('dashboard.ajax.images.search') }}";
  </script>
  <script src="{{ asset('dashboard/js/forms.js') }}"></script>
@endsection
