<div class="card-body">
  <div class="row">
    <div class="col-lg-3">
      <div class="form-group @if($errors->first('name')) has-error @endif">
        <label class="control-label" for="name">Name</label>
        {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name']) }}
        @if($errors->first('name')) <span class="help-block">{{ $errors->first('name') }}</span> @endif
      </div>

      <div class="form-group @if($errors->first('slug')) has-error @endif">
        <label class="control-label" for="slug">Slug</label>
        {{ Form::text('slug', old('slug'), ['class' => 'form-control', 'id' => 'slug']) }}
        @if($errors->first('slug')) <span class="help-block">{{ $errors->first('slug') }}</span> @endif
      </div>

      <div class="form-group @if($errors->first('title')) has-error @endif">
        <label class="control-label" for="title">Title</label>
        {{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title']) }}
        @if($errors->first('title')) <span class="help-block">{{ $errors->first('title') }}</span> @endif
      </div>

      <div class="form-group @if($errors->first('subtitle')) has-error @endif">
        <label class="control-label" for="subtitle">Subtitle</label>
        {{ Form::text('subtitle', old('subtitle'), ['class' => 'form-control', 'id' => 'subtitle']) }}
        @if($errors->first('subtitle')) <span class="help-block">{{ $errors->first('subtitle') }}</span> @endif
      </div>

      <div class="form-group @if($errors->first('avatar')) has-error @endif">
        <label for="avatar" class="control-label">Avatar</label>
        @if(isset($member))
          <div class="row">
            <div class="col-lg-4">
              <img src="{{ $member->avatar->url('thumb') }}">
            </div>
          </div>
          <a href="{{ $member->avatar->url() }}" target="_blank">Get Original Image</a>
          {!! Form::file('avatar', ['class' => 'custom-file-input']) !!}
        @else
          {!! Form::file('avatar', ['class' => 'custom-file-input', 'required' => true]) !!}
        @endif
        @if($errors->first('avatar')) <span class="help-block">{{ $errors->first('avatar') }}</span> @endif
      </div>

      <div class="form-group @if($errors->first('joined_at')) has-error @endif">
        <label class="control-label" for="joined_at">Joined At</label>
        {{ Form::date('joined_at', old('joined_at'), ['class' => 'form-control', 'id' => 'joined_at']) }}
        @if($errors->first('joined_at')) <span class="help-block">{{ $errors->first('joined_at') }}</span> @endif
      </div>

      <div class="form-group @if($errors->first('gold')) has-error @endif">
        <label class="control-label" for="gold">Member Status</label>
        {!! Form::select('gold', [0 => 'Current Member', 1 => 'Gold Member'], old('gold'),  ['class' => 'form-control', 'id' => 'gold', 'required' => true]) !!}
        @if($errors->first('gold')) <span class="help-block">{{ $errors->first('gold') }}</span> @endif
      </div>

      <div class="form-group @if($errors->first('facebook')) has-error @endif">
        <label class="control-label" for="facebook">Facebook Profile</label>
        {{ Form::text('facebook', old('facebook'), ['class' => 'form-control', 'id' => 'facebook']) }}
        @if($errors->first('facebook')) <span class="help-block">{{ $errors->first('facebook') }}</span> @endif
      </div>

      <div class="form-group @if($errors->first('twitter')) has-error @endif">
        <label class="control-label" for="twitter">twitter Profile</label>
        {{ Form::text('twitter', old('twitter'), ['class' => 'form-control', 'id' => 'twitter']) }}
        @if($errors->first('twitter')) <span class="help-block">{{ $errors->first('twitter') }}</span> @endif
      </div>

      <div class="form-group @if($errors->first('linkedin')) has-error @endif">
        <label class="control-label" for="linkedin">LinkedIn Profile</label>
        {{ Form::text('linkedin', old('linkedin'), ['class' => 'form-control', 'id' => 'linkedin']) }}
        @if($errors->first('linkedin')) <span class="help-block">{{ $errors->first('linkedin') }}</span> @endif
      </div>

      <div class="form-group @if($errors->first('github')) has-error @endif">
        <label class="control-label" for="github">Github Profile</label>
        {{ Form::text('github', old('github'), ['class' => 'form-control', 'id' => 'github']) }}
        @if($errors->first('github')) <span class="help-block">{{ $errors->first('github') }}</span> @endif
      </div>
    </div>
  </div>
</div>
