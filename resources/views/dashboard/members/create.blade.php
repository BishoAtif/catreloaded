@extends('dashboard.layouts.app')

@section('title')
  Add Member
@stop

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="box-body">
        {!! Form::open(['route' => 'dashboard.members.store', 'files' => 'true']) !!}
          @include('dashboard.members._form')
          <div class="form-actions">
            <div class="form-group">
              <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
              <button type="submit" class="btn btn-danger" onclick="window.location='{{ route('dashboard.members.index') }}';">Back</button>
            </div>
          </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
@endsection
