@extends('dashboard.layouts.app')

@section('title')
  Members
@stop

@section('styles')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
  <div class="box">
    <div class="box-body">
      <button type="button" class="btn bg-olive margin" onclick="window.location='{!! route('dashboard.members.create') !!}'">Add Member</button>
      <table id="members-table" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Title</th>
            <th>Joined Since</th>
            <th>Actions</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
@endsection

@section('footerScripts')
  <script>
    require(['jquery', 'datatables.net'], function($, DataTable){
      $('#members-table').DataTable( {
        serverSide: true,
        processing: true,
        ajax: {
          url: '{!! route('dashboard.ajax.members.index') !!}'
        },
        columns: [
          { data: 'name' },
          { data: 'title' },
          { data: 'joined_at'},
          {
            data: "actions",
            render: function(data){
              return htmlDecode(data);
            }
          }
        ]
      });
    });
  </script>
@endsection
