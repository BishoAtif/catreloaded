<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="treeview">
        {{ App\Helpers\MenuHelper::formatMenu() }}
      </li>
    </ul>
  </section>
</aside>
