<li>
  <a href="{{ array_key_exists('url', $element) ? route($element['url']) : '#' }}">
    <i class="fa fa-{{ array_key_exists('icon', $element) ? $element['icon'] : 'circle-o' }}"></i>{{ $element['name'] }}
  </a>
</li>
