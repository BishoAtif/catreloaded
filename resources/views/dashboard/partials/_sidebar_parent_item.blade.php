<li class="treeview">
  <a href="#">
    <i class="fa fa-{{ array_key_exists('icon', $element) ? $element['icon'] : 'share' }}"></i>
    <span>{{ $element['name'] }}</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
  </a>
  <ul class="treeview-menu">
    {{ App\Helpers\MenuHelper::formatElement($element['children']) }}
  </ul>
</li>
