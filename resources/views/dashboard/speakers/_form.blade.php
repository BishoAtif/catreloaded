<div class="row">
  <div class="col-lg-3">
    <div class="form-group @if($errors->first('name')) has-error @endif">
      <label class="control-label" for="name">Name</label>
      {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name']) }}
      @if($errors->first('name')) <span class="help-block">{{ $errors->first('name') }}</span> @endif
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-3">
    <div class="form-group @if($errors->first('avatar')) has-error @endif">
      <label for="avatar" class="control-label">Avatar</label>
      @if(isset($speaker))
        <div class="row">
          <div class="col-lg-4">
            <a href="{{ $speaker->avatar->url() }}" target="_blank"><img src="{{ $speaker->avatar->url('thumb') }}"></a>
          </div>
        </div><br>
        {!! Form::file('avatar', ['class' => 'custom-file-input']) !!}
      @else
        {!! Form::file('avatar', ['class' => 'custom-file-input', 'required' => true]) !!}
      @endif
      @if($errors->first('avatar')) <span class="help-block">{{ $errors->first('avatar') }}</span> @endif
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-3">
    <div class="form-group @if($errors->first('linkedin')) has-error @endif">
      <label class="control-label" for="linkedin">Linkedin Profile</label>
      {{ Form::text('linkedin', old('linkedin'), ['class' => 'form-control', 'id' => 'linkedin']) }}
      @if($errors->first('linkedin')) <span class="help-block">{{ $errors->first('linkedin') }}</span> @endif
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-9">
    <div class="form-group @if($errors->first('bio')) has-error @endif">
      <label class="control-label" for="bio">Content</label>
      {{ Form::textarea('bio', old('bio'), ['class' => 'form-control', 'id' => 'bio']) }}
      @if($errors->first('bio'))<span class="help-block">{{ $errors->first('bio') }}</span>@endif
      </div>
    </div>
</div>

@section('footerScripts')
  <script>
    require(['ckeditor'], function($){
      CKEDITOR.replace('bio', {
        language : 'ar'
      });
    });
  </script>
@endsection
