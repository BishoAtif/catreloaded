@extends('dashboard.layouts.app')

@section('title')
  Speakers
@stop

@section('styles')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
  <div class="box">
    <div class="box-body">
      <button type="button" class="btn bg-olive margin" onclick="window.location='{!! route('dashboard.speakers.create') !!}'">Add Speaker</button>
      <table id="speakers-table" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Avatar</th>
            <th>Name</th>
            <th>Actions</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
@endsection

@section('footerScripts') 
  <script>
    require(['jquery', 'datatables.net', 'dataTables'], function($, DataTable){
      $('#speakers-table').DataTable( {
        serverSide: true,
        processing: true,
        ajax: {
          url: '{!! route('dashboard.ajax.speakers.index') !!}'
        },
        columns: [
          { data: 'avatar',
            render: function(data){
              return '<img src="' + data + '">';
            }
          },
          { data: 'name' },
          {
            data: "actions",
            render: function(data){
              return htmlDecode(data);
             }
          }
        ]
      });
    });
  </script>
@endsection
