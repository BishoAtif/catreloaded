@extends('dashboard.layouts.app')

@section('title')
  Edit Event
@stop

@section('content')
  <div class="row">
    <div class="col-lg-12">
      {{ Form::model($speaker, ['route' => ['dashboard.speakers.update', $speaker->id], 'method' => 'PATCH', 'files' => 'true']) }}
        @include('dashboard.speakers._form')
        <div class="form-actions">
          <div class="form-group">
            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
            <button type="submit" class="btn btn-danger" onclick="window.location='{{ route('dashboard.speakers.index') }}';">Back</button>
          </div>
        </div>
      {{ Form::close() }}
    </div>
  </div>
@endsection
