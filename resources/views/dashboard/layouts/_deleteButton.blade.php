<form action="{!! $deleteUrl !!}" method="POST">
  {{ csrf_field() }}
  <input type="hidden" name="_method" value="delete">
  <button type="submit" class="btn btn-sm btn-flat btn-danger"><i class="fa fa-close"></i> Delete</button>
</form>
