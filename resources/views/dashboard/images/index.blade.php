@extends('dashboard.layouts.app')

@section('title')
  Images
@stop

@section('styles')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
  <div class="box">
    <div class="box-body">
      <button type="button" class="btn bg-olive margin" onclick="window.location='{!! route('dashboard.images.create') !!}'">Add Image</button>
      <table id="images-table" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Description</th>
            <th>Image</th>
            <th>Actions</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
@endsection

@section('footerScripts')
  <script>
    require(['jquery', 'datatables.net'], function($, DataTable){
      $('#images-table').DataTable( {
        serverSide: true,
        processing: true,
        ajax: {
          url: '{!! route('dashboard.ajax.images.index') !!}'
        },
        columns: [
          { data: 'description' },
          { data: 'image',
            render: function(data){
              return '<img src="' + data + '">';
            }
          },
          {
            data: "actions",
            render: function(data){
              return htmlDecode(data);
            }
          }
        ]
      });
    });
  </script>
@endsection
