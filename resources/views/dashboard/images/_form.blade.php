<div class="card-body">
  <div class="row">
    <div class="col-lg-3">
      <div class="form-group @if($errors->first('description')) has-error @endif">
        <label class="control-label" for="description">Description</label>
        {{ Form::text('description', old('description'), ['class' => 'form-control', 'id' => 'description']) }}
        @if($errors->first('description')) <span class="help-block">{{ $errors->first('description') }}</span> @endif
      </div>

      <div class="form-group @if($errors->first('image')) has-error @endif">
        <label for="image" class="control-label">Image</label>
        @if(isset($image))
          <div class="row">
            <div class="col-lg-4">
              <img src="{{ $image->image->url('thumb') }}">
            </div>
          </div>
          <a href="{{ $image->image->url() }}" target="_blank">Get Original Image</a>
          {!! Form::file('image', ['class' => 'custom-file-input']) !!}
        @else
          {!! Form::file('image', ['class' => 'custom-file-input', 'required' => true]) !!}
        @endif
        @if($errors->first('image')) <span class="help-block">{{ $errors->first('image') }}</span> @endif
      </div>
      
    </div>
  </div>
</div>
